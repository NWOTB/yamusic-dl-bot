# Yandex.Music Download Bot

## Overview

A Telegram bot that returns Audio (mp3) right after you sent there link to track you wish to download.

## Building

### Prerequisities
- NodeJS v20
- yarn
- Repository cloned to your local machine

### Install dependencies
```
cd app
yarn
```

### Make .env file

```
ENVIRONMENT=    # environment name (example: testing)
SERVER_ADDRESS= # server address
SSH_USER=       # gitlab-ci user (default: gitlab_ci)
BOT_TOKEN=      # [required] tg bot token
ALLOWED_IDS=    # [required] tg access list (example: user1:12345678,user2:87654321)
YTOKEN=         # [required] yandex music token
YUID=           # [required] yandex music UID
```

By default it reads env file from ```../private/envs/testing.env```. You can change path in ```app.ts```.

### Run

```yarn start```
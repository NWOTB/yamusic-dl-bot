import { AppError } from "../error";
import { logger } from "../logger";

export namespace Helpers {

    export async function parseTrackId(ctx: any) {
        const text = ctx.message.text;

        let trackId: number;
        try {
            trackId = +text.split('track/')[1].split('?')[0];
        } catch (e) {
            throw new AppError(
                'PARSE_TRACK_ID_IN_URL_ERROR',
                'Failed to parse track id in the track URL.',
                e
            )
        }
        return trackId;
    }

    export function parseACL(ALLOWED_IDS: string) {
        let allowedIds:number[] = [];
        try {
        allowedIds = ALLOWED_IDS.split(',').map((v) => +(v.split(':')[1]));
        logger.debug('ACL:', allowedIds);
        } catch(e) {
        throw new AppError(
            'ALLOWED_IDS_PARSING_ERROR',
            'Parsing ALLOWED_IDS failed.',
            e);
        }
        return allowedIds;
    }
}
import { InputFile } from "grammy";
import Jimp from "jimp";
import { YMApi } from "ym-api-meowed";
import { AppError } from '../error';
import { logger } from "../logger";

export class YTrack {
  public title: string;
  public artists: string;
  private _thumbnailURL: string;
  get thumbnail(): Promise<{ thumbnail?: InputFile }> {
    const _self = this;
    return new Promise(async (resolve, reject) => {
      try {
        logger.debug(`Fetching thumbnail by URL: ${_self._thumbnailURL}`);

        const thumbnailFile = await (await fetch(_self._thumbnailURL)).arrayBuffer();
        logger.debug(`Thumbnail file size: ${Math.ceil(thumbnailFile.byteLength/1000)}kb`);

        const resized = await (await Jimp.read(Buffer.from(thumbnailFile)))
          .resize(320, 320)
          .quality(80) // compress image to complain max size 200kb
          .getBufferAsync(Jimp.MIME_JPEG);
        
        logger.debug(`Thumbnail compressed to ${Math.ceil(resized.length/1000)}kb (max: 200kb)`);

        resolve({
          thumbnail: new InputFile(new Uint8Array(resized))
        });
      } catch(e) {
        reject({})
      }
    });
  }
  private _trackURL: string;

  get track(): Promise<InputFile> {
    const _self = this;
    return new Promise(async (resolve, reject) => {
      try {
        logger.debug(`Fetching track by URL: ${_self._trackURL}`);

        const trackFile = await (await fetch(_self._trackURL)).arrayBuffer();
        logger.debug(`Track file size: ${Math.ceil(trackFile.byteLength/1000)}kb`);

        const filename = `${_self.artists} - ${_self.title}`;
        logger.debug(`Filename: ${filename}`);

        resolve(new InputFile(new Uint8Array(trackFile), filename));
      } catch(e) {
        reject(new AppError(
          "DOWNLOAD_TRACK_ERROR",
          'Unable to download track.',
          e
        ))
      }
    });
  }

  constructor({ title, artists, thumbnailURL, trackURL }: {
    title: string,
    artists: string,
    thumbnailURL: string | null,
    trackURL: string,
  }) {
    this.title = title;
    this.artists = artists;
    this._thumbnailURL = thumbnailURL;
    this._trackURL = trackURL;
  }
};

export class YDownloader {
  public api: YMApi;
  constructor(
    public token: string,
    public uid: number
    ) {
    this.api = new YMApi();
  }

  async getTrackMetaById(trackId: number): Promise<YTrack> {
    const _self = this;

    await _self.api.init({
      access_token: _self.token,
      uid: _self.uid
    });

    const track =  await _self.api.getTrack(trackId);
    const title = track?.[0]?.title ?? 'Unknown title';
    const artists = track?.[0]?.artists?.map((v) => v.name).join(', ') ?? 'Unknown artist';
    const thumbnailURL = 'https://' + (track[0]?.coverUri?.replace('%%', '') ?? '') +'m1000x1000';

    let trackURL: string = null;
    try {
      const downloadInfo = await _self.api.getTrackDownloadInfo(trackId);
      const mp3Tracks = downloadInfo
        .filter((r) => r.codec === "mp3")
        .sort((a, b) => b.bitrateInKbps - a.bitrateInKbps);
      const hqMp3Track = mp3Tracks[0];
      trackURL = await _self.api.getTrackDirectLink(
        hqMp3Track.downloadInfoUrl
      );
    } catch(e) {
      throw new AppError(
        "YM_GET_TRACK_LINK_ERROR",
        'Unable to get link to track.',
        e);
    }

    return new YTrack({
      title,
      artists,
      thumbnailURL,
      trackURL
    });
  }
}

import winston from 'winston';

// Initialize logger
export const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.simple(),
    transports: [new winston.transports.Console()],
});

// Add logging to console
// logger.add(new winston.transports.Console({
//     format: winston.format.combine(
//         winston.format.prettyPrint(),
//         winston.format.splat(),
//         //winston.format.json()
//     )
// }));
/*
import winston from "winston";
import Transport from "winston-transport";

const { MESSAGE } = require("triple-beam");
const level = process.env.LOG_LEVEL || "info";

class SimpleConsoleTransport extends Transport {
  log = (info: any, callback: any) => {
    setImmediate(() => this.emit("logged", info));

    console.log(info[MESSAGE]);

    if (callback) {
      callback();
    }
  };
}

export const logger = winston.createLogger({
  level,
  defaultMeta: {},
  transports: [new SimpleConsoleTransport()]
});
*/

export type AppErrorName = 
    | 'YM_GET_TRACK_LINK_ERROR'
    | 'ALLOWED_IDS_PARSING_ERROR'
    | 'PARSE_TRACK_ID_IN_URL_ERROR'
    | 'DOWNLOAD_TRACK_ERROR';

export class AppError extends Error {
    constructor(
        public name: AppErrorName,
        public message: string,
        public cause: any) {
        super();
    }
}

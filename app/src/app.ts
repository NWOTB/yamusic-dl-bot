import dotenv from 'dotenv';
import { Bot, GrammyError, HttpError } from 'grammy';
import path from 'path';
import { AppError } from './error';

import { guard, isUserHasId, reply } from 'grammy-guard';
import { Helpers } from './helpers';
import { logger } from './logger';
import { YDownloader } from './ydownloader';


(async () => {
  if(!process.env['ENVIRONMENT']) {
    logger.warn("*** RUNNING IN TESTING EVIRONMENT ***");

    // Load environment 'testing' variables
    dotenv.config({
      path: path.resolve(process.cwd(), '../private/envs/.testing.env')
    });
  }

  // Extract environment variables
  const {
    YTOKEN,
    YUID,
    BOT_TOKEN,
    ALLOWED_IDS
  } = process.env;

  const bot = new Bot(BOT_TOKEN);
  const acl = Helpers.parseACL(ALLOWED_IDS);
  const ydl = new YDownloader(YTOKEN, +YUID);

  // Guard from not allowed users
  bot.use(guard(
    isUserHasId(...acl),
    reply('Напиши @foreverjunior чтобы он тебя добавил в список пацанов :)')
  ));

  // Handle /start command
  bot.command('start', async (ctx) => {
    await ctx.reply('Скинь сюда ссылку на трек и получи mp3 в ответ :)');
  });

  bot.on("message:text", async (ctx) => {  
    const trackId = await Helpers.parseTrackId(ctx);
    const track = await ydl.getTrackMetaById(trackId);

    logger.info('Will send an audio');
    await ctx.replyWithAudio(
      await track.track,
      Object.assign({
        title: track.title,
        performer: track.artists,
        reply_parameters: {
          message_id: ctx.message.message_id
        }
      }, await track.thumbnail));
    logger.info('Audio sent');
  });

  bot.catch(async (err) => {
    const ctx = err.ctx;
    logger.error(`Grammy: Error while handling update ${ctx.update.update_id}:`);
    const e = err.error;
    if (e instanceof GrammyError) {
      logger.error("Grammy: Error in request:", e.description);
    } else if (e instanceof HttpError) {
      logger.error("Grammy: Could not contact Telegram:", e);
    } else if(e instanceof AppError) {
      logger.error("App: Application error:", e);

      if(e.name === 'PARSE_TRACK_ID_IN_URL_ERROR') {
        await ctx.reply('Ошибка: не удаётся извлечь id трека.');
      } else {
        await ctx.reply(`Ошибочка вышла:\n${e.name}\n\nСообщите @foreverjunior`);
      }
    } else {
      logger.error("Grammy: Unknown error:", e);
    }
  });

  bot.start();

  logger.info('Bot started asynchronously!');
})();
